import { notification } from 'antd';

const NotificationMessage = (type,displayMessage) => {
  notification[type]({
    message: 'User Details',
    description: displayMessage,
    notofication: 1
  });
};

export default NotificationMessage;